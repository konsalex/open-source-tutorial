var starwarsnames = require('./starwars-names.json');

module.exports = {
    all: starwarsnames,
    random: starwarsnames.slice(1, 5)
}
