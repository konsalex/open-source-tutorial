var expect = require('chai').expect
var starWars = require('./index.js');

describe('starwars-names', function () {

    describe('all', function () {

        it('should be an all array of strings', function () {

            expect(starWars.all).to.satisfy(isArrayOfStrings);

            function isArrayOfStrings(array) {

                return array.every(function (item) {

                    return typeof item === 'string'

                });

            }
        });

        it('should contain `Luke Skywalker`!', function () {

            expect(starWars.all).to.include('Luke Skywalker');

        });

    });

    describe('random', function () {

        it('should return random item', function () {
            var randomItem = starWars.random;
            // To include.members helps find spread values across list
            expect(starWars.all).to.include.members(randomItem);
        });

    })

});